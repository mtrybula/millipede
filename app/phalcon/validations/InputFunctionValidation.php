<?php
/**
 * @package Millipede\Validations
 * @author Maciej Trybuła <mtrybula@divante.pl>
 * @copyright 2018 Divante Sp. z o.o.
 * @license See LICENSE_DIVANTE.txt for license details.
 */

namespace Millipede\Validations;

use Phalcon\Validation;

/**
 * Class InputFunctionValidation
 */
class InputFunctionValidation extends Validation
{
    public function initialize()
    {

    }
}
